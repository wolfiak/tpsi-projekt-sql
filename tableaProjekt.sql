CREATE TABLE uzytkownik (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
firstname VARCHAR(30) NOT NULL,
lastname VARCHAR(30) NOT NULL,
email VARCHAR(50),
password VARCHAR(50),
ulica VARCHAR(30) NOT NULL,
miasto VARCHAR(100) NOT NULL,
kod VARCHAR(500)
);
CREATE TABLE pracownik (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
firstname VARCHAR(30) NOT NULL,
lastname VARCHAR(30) NOT NULL,
email VARCHAR(50),
password VARCHAR(50),
ulica VARCHAR(30) NOT NULL,
miasto VARCHAR(100) NOT NULL,
kod VARCHAR(500)
);


CREATE TABLE sesja (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
klucz VARCHAR(60) NOT NULL,
id_klienta INT(6) NOT NULL,

);




CREATE TABLE ksiazka (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
tytul VARCHAR(30) NOT NULL,
url VARCHAR(100) NOT NULL,
liczba_stron INT(6) NOT NULL,
cena INT(6),
ilosc INT(6),
wydanie VARCHAR(100),
id_autora INT(6) NOT NULL,
id_wydawnictwa INT(6) NOT NULL
);
CREATE TABLE autor (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
imie VARCHAR(30) NOT NULL,
nazwisko VARCHAR(30) NOT NULL,
url VARCHAR(100) NOT NULL,
opis VARCHAR(500)
);

CREATE TABLE wydawnictwo (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
nazwa VARCHAR(30) NOT NULL,
ulica VARCHAR(30) NOT NULL,
miasto VARCHAR(100) NOT NULL,
kod VARCHAR(500)
);
CREATE TABLE sprzedaz (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
id_klienta INT(6) NOT NULL,
id_ksiazki INT(6) NOT NULL,
ilosc INT(6) NOT NULL,
suma INT(6) NOT NULL

);
INSERT INTO wydawnictwo VALUES(
1,
'Dom Wydawniczy Rebis',
'uliczna',
'Siedlce',
'08-110'
)
INSERT INTO wydawnictwo VALUES(
2,
'Edipresse Książki',
'uliczna',
'Siedlce',
'08-110'
)




INSERT INTO ksiazka VALUES(
2,
'Zdrowe koktajle',
'http://ecsmedia.pl/c/zdrowe-koktajle-w-iext49189898.jpg',
200,
25,
10,
'miekka',
2,
2
)
INSERT INTO autor VALUES(
2,
'Ewa',
'Chodakowska',
'http://www.zeberka.pl/img_new/2017/03/14/kapif-k713677f.jpg',
'Ewa Chodakowska-Kavoukis urodziła się 24 lutego 1982 roku w Sanoku. Jest najbardziej znaną polską trenerką osobistą i specjalistką od kształtowania ciała. Jej programy ćwiczeń i żywienia stały się prawdziwym fenomenem, a charyzma i umiejętności motywacyjne sprawiły, że zaufało jej miliony Polek, odzyskując dobrą formę oraz doskonałą sylwetkę. 
'
)

INSERT INTO ksiazka VALUES(
1,
'13 powodow',
'http://ecsmedia.pl/c/13-powodow-w-iext48472449.jpg',
200,
25,
10,
'miekka',
1,
1
)
INSERT INTO autor VALUES(
1,
'Jay',
'Asher',
'https://upload.wikimedia.org/wikipedia/commons/2/24/Jay_asher_2011.jpg',
'Jay Asher to amerykański pisarz współczesnej powieści dla młodzieży. Był jednym z głównych publikacji w gatunku literatury młodych dorosłych . Opublikowane prace Trzynaście powodów'
)